const express = require('express');
const cors = require('cors');

const app = express();
const port = 3000;

app.use(cors());
app.use(express.json());

app.post('/api/reviews', (req, res) => {
  const { rating, review, selection } = req.body;
  console.log(`Received review: Rating: ${rating}, Review: ${review}, Selection: ${selection}`);
  res.json({ success: true });
});

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
