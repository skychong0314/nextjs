import React, { useState } from "react";
import Rating from "react-rating";
import axios from "axios";

export default function ReviewForm() {
  const [rating, setRating] = useState(0);
  const [review, setReview] = useState("");
  const [selection, setSelection] = useState("");
  const [imageSrc, setImageSrc] = useState("");

  const handleRatingChange = (value) => {
    setRating(value);
  };

  const handleReviewChange = (event) => {
    setReview(event.target.value);
  };

  const handleSelectionChange = (event) => {
    setSelection(event.target.value);
    switch (event.target.value) {
      case "option1":
        setImageSrc("http://clipart-library.com/images/qTB7onyT5.jpg");
        break;
      case "option2":
        setImageSrc("http://clipart-library.com/data_images/162018.jpg");
        break;
      case "option3":
        setImageSrc("http://clipart-library.com/images/kcKnKXXGi.png");
        break;
      default:
        setImageSrc("");
        break;
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      await axios.post("/api/reviews", { rating, review, selection });
      alert("Review submitted successfully!");
      setRating(0);
      setReview("");
      setSelection("");
      setImageSrc("");
    } catch (error) {
      console.error(error);
      alert("Error submitting review.");
    }
  };

  return (
    <div className="container">
      <div className="left">
        <h1>Movie</h1>
        <label htmlFor="selection">Select an Movie:</label>
        <select id="selection" value={selection} onChange={handleSelectionChange}>
          <option value="">Movie you watched</option>
          <option value="option1">Badman</option>
          <option value="option2">Spiderman</option>
          <option value="option3">Superman</option>
        </select>
        {imageSrc && <img src={imageSrc} alt="Your Image" />}
      </div>
      <div className="right">
        
        <form onSubmit={handleSubmit}>
          <div>
            <label htmlFor="rating"><b>Rating:</b></label>
            <Rating
              id="rating"
              initialRating={rating}
              onChange={handleRatingChange}
              emptySymbol={<i className="far fa-star fa-2x" />}
              fullSymbol={<i className="fas fa-star fa-2x" />}
            />
          </div>
          <br/>  <br/>  <br/>
          <div>
          <h2><b>Leave a Review</b></h2>
            <label htmlFor="review">Review:  </label>
            <textarea
            placeholder="Comments?"
              id="review"
              value={review}
              onChange={handleReviewChange}
              rows={5}
            />
          </div>
          <br/>  <br/>  <br/>
          <button type="submit">Submit</button>
        </form>
      </div>
    </div>
  );
}
