const express = require("express");
const router = express.Router();
const axios = require("axios");

router.get("/", async (req, res) => {
  try {
    const response = await axios.get("http://localhost:3000/api/reviews");
    const reviews = response.data;
    res.json(reviews);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Unable to retrieve reviews" });
  }
});

router.post("/", async (req, res) => {
  const { rating, review, selection } = req.body;
  try {
    await axios.post("http://localhost:3000/api/reviews", { rating, review, selection });
    res.json({ success: true });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Unable to submit review" });
  }
});

module.exports = router;
