const express = require('express');
const cors = require('cors');

const app = express();
const port = 3000;

app.use(cors());
app.use(express.json());

let reviews = [];

app.post('/api/reviews', (req, res) => {
  const { rating, review, selection } = req.body;
  console.log(`Received review: Rating: ${rating}, Review: ${review}, Selection: ${selection}`);
  reviews.push({ rating, review, selection });
  res.json({ success: true });
});

app.get('/api/reviews', (req, res) => {
  res.json(reviews);
});

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
